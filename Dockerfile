FROM ros:latest

SHELL ["/bin/bash", "-c"]

RUN apt-get update && apt-get install --no-install-recommends -y \
	ros-melodic-gazebo-* wget python-pip libeigen3-dev
RUN apt-get install ros-melodic-mavros ros-melodic-mavros-extras -y
RUN apt-get install python-catkin-tools python-rosinstall-generator -y
RUN apt-get install ros-melodic-mavros ros-melodic-mavros-extras
RUN apt-get install python-catkin-tools python-rosinstall-generator -y
RUN apt-get install -y python-lxml
RUN apt-get install -y python-future
RUN apt install nano

RUN wget https://raw.githubusercontent.com/PX4/Devguide/master/build_scripts/ubuntu_sim_common_deps.sh
RUN source ubuntu_sim_common_deps.sh

RUN wget https://raw.githubusercontent.com/mavlink/mavros/master/mavros/scripts/install_geographiclib_datasets.sh
RUN sh install_geographiclib_datasets.sh

RUN mkdir -p /root/catkin_ws/src
WORKDIR /root/catkin_ws
RUN catkin init
RUN ls -al
RUN wstool init src

RUN rosinstall_generator --rosdistro kinetic mavlink | tee /tmp/mavros.rosinstall
RUN rosinstall_generator --upstream mavros | tee -a /tmp/mavros.rosinstall

RUN wstool merge -t src /tmp/mavros.rosinstall
RUN wstool update -t src -j4
RUN rosdep install --from-paths src --ignore-src -y

LABEL com.nvidia.volumes.needed="nvidia_driver"
ENV PATH /usr/local/nvidia/bin:${PATH}
ENV LD_LIBRARY_PATH /usr/local/nvidia/lib:/usr/local/nvidia/lib64:${LD_LIBRARY_PATH}

# another step:
# clone Firmware
# install fastrtps
# catkin build -j4
# pip install jinja
# pip install numpy toml