# PX4 Gazebo ROS Docker

Dockerfile for PX4 SITL with Gazebo and ROS integration. Early version.

## Notes

I'm aware that PX4 itself provides their Dockerfiles with ros and simulation installed, but as I tried to run the simulation, somehow it crashes my machine at `make` command. I don't know the reason yet, but it may correlate with the Docker image provided by PX4 have so much packages installed, and this Dockerfile of mine only covers bare minimum to run simulation, maybe some packages break at some point. For now I stick to my build.

## Prequisite

`docker` and `nvidia-docker`, but you can always remove the nvidia in `launch.sh` file

## Installation

Assuming using root user inside container. Also using root priviledge to run docker in your machine. Clone this repo and open terminal on the folder, and then build the docker image:

```
docker build -t rgs-t .
```

Then run the image:

```
chmod +x launch.sh
./launch.sh`
```

And we need to finish the configuration.

### Installing FastRTPS

Copy the script `install_FastRTPS.sh` from this repo to `/root/` and run it:

```
sh install_FastRTPS.sh
```

### Clone PX4 Firmware

Go to `/root/` and clone the PX4 Firmware:

```
git clone https://github.com/PX4/Firmware.git
```

### Finishing catkin_build

Go to `/root/catkin_ws/` and then run this command:

```
catkin build -j4
```

Note that `-j4 `can be anything to `-j12` depending on the number of CPUs of your hardware. `-j4` ensuring this command won't crash on my system.

### Install another python packages

```
pip install jinja2
pip install numpy toml
```

## Running the Simulation

Go to the Firmware directory (in this case, `/root/Firmware/`) and run the command depending on the vehicle you want to simulate, in my case, I want to simulate standard VTOL:

```
make px4_sitl_default gazebo_standard_vtol
```

A Gazebo window will be spawned with a VTOL inside. Actually you can start to play around with the VTOL using the terminal, you can try takeoff but it will soon land again as it's still on AUTO.RTL mode.

```
commander takeoff
```

### Integrate with ROS

Use another terminal, run the `docker exec` to access the currently running container.

Use `docker ps` to get the name of the running container (our image is called `rgs-t`), and then run this code as a root:

```
docker exec -it <Container Name> bash
```

Source the `setup.bash` in `catkin_ws`:

```
source /root/catkin_ws/devel/setup.bash
```

Launch MAVROS:

```
roslaunch mavros px4.launch fcu_url:="udp://:14540@127.0.0.1:14557"
```

You can test the ROS integration with another terminal.

```
rostopic list
```

Remember that everytime we use ROS we have to source the `setup.bash` in `catkin_ws/devel/` folder

## Credits and References

https://github.com/facontidavide/ros-docker-gazebo : I used their `Dockerfile` and `launch.sh` as reference.

https://dev.px4.io/en/setup/dev_env_linux.html : The install_fastRTPS.sh script is actually came from https://raw.githubusercontent.com/PX4/Devguide/master/build_scripts/ubuntu_sim_common_deps.sh but we only need the installation part.

https://dev.px4.io/en/simulation/gazebo.html

https://dev.px4.io/en/simulation/ros_interface.html

# Appendix 1 : Wind Simulation

In this part of the documentation I will demonstrate on how to simulate wind in this particular SITL simulation. Models within PX4 simulation have wind plugin integrated inside their sdf model, but they aren't activated yet, so we have to edit the wind plugin part of the model configuration.

Source code for the models are in `/Firmware/Tools/sitl_gazebo/models/`, we can navigate there and choose your particular vehicle, note that this method affects the model vehicle itself, so if you simulate several vehicle types at the same time you need to edit all of their model configurations. This also means that we are not simulate wind to the world. For this example, we use `standard_vtol`.

Inside `standard_vtol` folder, open the `standard_vtol.sdf` file. Scroll down to this part:

```
<!--
<plugin name='wind_plugin' filename='libgazebo_wind_plugin.so'>
  <frameId>base_link</frameId>
  <linkName>base_link</linkName>
  <robotNamespace/>
  <xyzOffset>1 0 0</xyzOffset>
  <windDirection>0 1 0</windDirection>
  <windForceMean>0.7</windForceMean>
  <windGustDirection>0 0 0</windGustDirection>
  <windGustDuration>0</windGustDuration>
  <windGustStart>0</windGustStart>
  <windGustForceMean>0</windGustForceMean>
</plugin>
-->
```

This is the part where we have to edit to simulate wind force applied to the vehicle. To start, remove `<!--` and `-->`. While there are no documentation regarding this plugin, we can use the source code of the `libgazebo_wind_plugin.so` as our reference, it's located in `/Firmware/Tools/sitl_gazebo/src/gazebo_wind_plugin.cpp`.

If we look closely, we could provide more variables to this plugin, such as `windDirectionMean` (which is kinda confusing, because in the commented code it's `windDirection`), `windForceMax`, `windDirectionVariance`, `windForceVariance` (It is unclear whether these variable needs to be provided, while I analyze the source, I conclude that we shouldn't leave these blank), `windForceMean` etc. For now, we leave wind gust variables as I can't really use it as it requires me to time the simulation.

For this example, I use these configuration:

```
<plugin name='wind_plugin' filename='libgazebo_wind_plugin.so'>
  <frameId>base_link</frameId>
  <linkName>base_link</linkName>
  <robotNamespace/>
  <xyzOffset>1 0 0</xyzOffset>
  <windDirectionMean>0.0 1.0 0.0</windDirectionMean>
  <windDirectionVariance>0.0</windDirectionVariance>
  <windForceMean> 0.27470608</windForceMean>
  <windForceMax>0.3</windForceMax>
  <windForceVariance>1.0</windForceVariance>
  <windGustDirection>0 0 0</windGustDirection>
  <windGustDuration>0</windGustDuration>
  <windGustStart>0</windGustStart>
  <windGustForceMean>0</windGustForceMean>
</plugin>
```

In short, we provide mean to the wind force (which is in Newton unit, this isn't explicitly provided, various source suggests that Gazebo use SI) and then the plugin itself will generate some normal distribution within it's variance, but it won't cross the maximum value. For my example, it's 0.27470608 N to simulate calm wind (https://en.wikipedia.org/wiki/Beaufort_scale) which is calculated from 0.5 mph wind speed with this method: https://sciencing.com/convert-wind-speed-force-5985528.html . The `windDirectionMean` field needs to be provided with 3 values, which represents where the wind blows, and how big the force depending on the other direction, basically this denotes wind direction vector. I believe this variable is in ENU convention in global frame, which means `1.0 1.0 0.0` will simulate wind that flows to North East Direction, there's also a `windDirectionVariance` that will create chaotic wind that blows in basically every directions if it's not set to 0.0, as their function names implies in the source code, the direction of the wind is also distributed (normal distribution).

After saving your change in the code, you can start the simulation again with:

```
make px4_sitl_default gazebo_standard_vtol
```

The wind will become disturbance even when the VTOL is currently not moving if the force is too strong, the wind force will be on full effect when the VTOL is in the air, that's why I made a quick and dirty code to simulate an offboard control of the VTOL using python / rospy, which I provided in `extras/demo.py` in this repo.

# Appendix 2 : Waypoint Navigation + Ground Control Integration

In PX4 Fiight Stack, you need to connect the autopilot program with ground control station to use waypoint navigation, to do waypoint navigation, we need to send our waypoint details, such as where is the waypoint in latitude, longitude, and altitude, also what should it do when it reaches the waypoint (land, loiter, etc.) which is needed to activate AUTO.MISSION flight mode.

To connect PX4 with Ground Control Station, first we choose our GCS application, I use APM Planner 2 as I used to this application already and it's a simple and uncomplicated program. You can run the GCS application outside the docker container (in your main machine), to connect PX4 to GCS, we need to configure PX4 to transmit telemetry data to a certain IP and/or port. To do this, open  `Firmware/ROMFS/px4fmu_common/init.d-posix/rcS`, and at line 226 there is this part:

```
# GCS link
mavlink start -x -u $udp_gcs_port_local -r 4000000
mavlink stream -r 50 -s POSITION_TARGET_LOCAL_NED -u $udp_gcs_port_local
mavlink stream -r 50 -s LOCAL_POSITION_NED -u $udp_gcs_port_local
mavlink stream -r 50 -s GLOBAL_POSITION_INT -u $udp_gcs_port_local
mavlink stream -r 50 -s ATTITUDE -u $udp_gcs_port_local
mavlink stream -r 50 -s ATTITUDE_QUATERNION -u $udp_gcs_port_local
mavlink stream -r 50 -s ATTITUDE_TARGET -u $udp_gcs_port_local
mavlink stream -r 50 -s SERVO_OUTPUT_RAW_0 -u $udp_gcs_port_local
mavlink stream -r 20 -s RC_CHANNELS -u $udp_gcs_port_local
mavlink stream -r 10 -s OPTICAL_FLOW_RAD -u $udp_gcs_port_local
```

add `-t <your computer ip>` after `mavlink start -x -u $udp_gcs_port_local -r 4000000`. Your IP corresponds to which network that your computer connect to the docker container, to find out the IP address, use `ifconfig` and find the IP that correspond with Docker interface, in my case, Docker's network interface is just simply `docker0`. This will make PX4 transmit the telemetry data to the target IP in a certain port (default port is 14550). The protocol used in this transfer is UDP, so, in your Ground Control Station, you only need to make GCS listen to port 14550 UDP.

After that you can provide waypoints for the autopilot program and switch it's flight mode to AUTO.MISSION. I provided a demo code in `extra/demo_waypoint.py`, run this code after connecting the PX4 with GCS.

## Credits and References

https://discuss.px4.io/t/how-to-make-qgcontrol-connect-to-gazebo-simulation-instance-in-another-host-in-same-lan/9941