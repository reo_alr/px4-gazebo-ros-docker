import rospy
import tf
import math
import mavros_msgs.msg
import mavros_msgs.srv

class GlobalVars():
	pass

G = GlobalVars()

G.curr_state = mavros_msgs.msg.State()

def state_cb(data):
	G.curr_state = data

rospy.init_node("demo_wp", anonymous = True)

G.add_wp = rospy.ServiceProxy("/mavros/mission/push", mavros_msgs.srv.WaypointPush)
G.set_mode = rospy.ServiceProxy("/mavros/set_mode", mavros_msgs.srv.SetMode)
G.arm = rospy.ServiceProxy("/mavros/cmd/arming", mavros_msgs.srv.CommandBool)

G.state_sub = rospy.Subscriber("/mavros/state", mavros_msgs.msg.State, state_cb)

G.rate = rospy.Rate(10)

while not rospy.is_shutdown() and not G.curr_state.connected:
	G.rate.sleep()

wp = mavros_msgs.srv.WaypointPush()
# wp.start_index = 0
wps = []
wp_one = mavros_msgs.msg.Waypoint()
wp_one.frame = wp_one.FRAME_GLOBAL
wp_one.is_current = True
wp_one.autocontinue = True
wp_one.command = 16 # MAV_CMD_NAV_LAND
wp_one.param1 = 5.0
wp_one.param2 = 0.0
wp_one.param3 = 1.0
wp_one.param4 = float('nan')
wp_one.x_lat = 47.397742
wp_one.y_long = 8.5457
wp_one.z_alt = 10.0
wps.append(wp_one)

wp_one = mavros_msgs.msg.Waypoint()
wp_one.frame = wp_one.FRAME_GLOBAL
wp_one.is_current = False
wp_one.autocontinue = True
wp_one.command = 21 # MAV_CMD_NAV_LAND
wp_one.param1 = 0.0
wp_one.param2 = 0
#wp_one.param3 = 1.0
wp_one.param4 = float('nan')
wp_one.x_lat = 47.397742
wp_one.y_long = 8.5457
wp_one.z_alt = 10.0
wps.append(wp_one)
# wp.waypoints = wps

G.add_wp(0, wps)

G.arm(True)
G.set_mode(0, "AUTO.MISSION")

rospy.spin()