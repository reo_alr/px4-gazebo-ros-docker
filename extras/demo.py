import rospy
import tf
import math
import geometry_msgs.msg
import mavros_msgs.msg
import mavros_msgs.srv
import std_msgs.msg

class GlobalVars():
	pass

G = GlobalVars()

# current state
G.curr_state = mavros_msgs.msg.State()
G.pose_to_pub = geometry_msgs.msg.PoseStamped()
G.target_to_pub = mavros_msgs.msg.PositionTarget()
G.target_att_to_pub = mavros_msgs.msg.AttitudeTarget()
# G.pose_publish_ok = False

# callback functions
def state_cb(data):
	# print "get"
	G.curr_state = data
	# if G.pose_publish_ok:
	# 	G.position_pub.publish(G.pose_to_pub)

rospy.init_node("demo", anonymous = True)

G.state_sub = rospy.Subscriber("/mavros/state", mavros_msgs.msg.State, state_cb)

G.set_mode = rospy.ServiceProxy("/mavros/set_mode", mavros_msgs.srv.SetMode)
G.arm = rospy.ServiceProxy("/mavros/cmd/arming", mavros_msgs.srv.CommandBool)

G.position_pub = rospy.Publisher("mavros/setpoint_position/local", geometry_msgs.msg.PoseStamped, queue_size = 10)
# G.position_pub = rospy.Publisher("mavros/setpoint_raw/local", mavros_msgs.msg.PositionTarget, queue_size = 10)
# G.att_pub = rospy.Publisher("mavros/setpoint_raw/attitude", mavros_msgs.msg.AttitudeTarget, queue_size = 10)

G.rate = rospy.Rate(10)

while not rospy.is_shutdown() and not G.curr_state.connected:
	G.rate.sleep()

pose = geometry_msgs.msg.PoseStamped()
pose.pose.position.x = 0
pose.pose.position.y = 0
pose.pose.position.z = 10.0
G.pose_to_pub = pose

G.pose_publish_ok = True

# target = mavros_msgs.msg.PositionTarget()
# target.position.x = 0.0
# target.position.y = 0.0
# target.position.z = 10.0
# target.coordinate_frame = target.FRAME_LOCAL_OFFSET_NED
# target.type_mask = target.IGNORE_VX | target.IGNORE_VY | target.IGNORE_VZ | target.IGNORE_AFX | target.IGNORE_AFY | target.IGNORE_AFZ | target.FORCE | target.IGNORE_YAW | target.IGNORE_YAW_RATE
# G.target_to_pub = target

for x in range(100):
	G.position_pub.publish(G.pose_to_pub)
	# G.position_pub.publish(G.target_to_pub)
	G.rate.sleep()

# r = G.set_mode(0, "OFFBOARD")
while not G.curr_state.mode == "OFFBOARD":
	print "Trying to switch mode"
	G.set_mode(0, "OFFBOARD")
	# G.position_pub.publish(G.target_to_pub)
	G.position_pub.publish(G.pose_to_pub)
	G.rate.sleep()

while not G.curr_state.armed:
	print("Trying to Arm")
	G.arm(True)
	# G.position_pub.publish(G.target_to_pub)
	G.position_pub.publish(G.pose_to_pub)
	G.rate.sleep()

G.tick = 0

while G.tick < 100:
	print "Hold pos 10m"
	# G.position_pub.publish(G.target_to_pub)
	G.position_pub.publish(G.pose_to_pub)
	G.rate.sleep()
	G.tick += 1

# target_att = mavros_msgs.msg.AttitudeTarget()
# quat = tf.transformations.quaternion_from_euler(math.radians(0.0), math.radians(0.0), math.radians(180.0))
# target_att.orientation.x = quat[0]
# target_att.orientation.y = quat[1]
# target_att.orientation.z = quat[2]
# target_att.orientation.w = quat[3]
# target_att.thrust = 1.0
# target_att.body_rate.z = 1.0
# target_att.type_mask = target_att.IGNORE_ROLL_RATE | target_att.IGNORE_PITCH_RATE | target_att.IGNORE_THRUST | target_att.IGNORE_ATTITUDE
# G.target_att_to_pub = target_att

pose = geometry_msgs.msg.PoseStamped()
pose.pose.position.x = 0
pose.pose.position.y = 0
pose.pose.position.z = 10.0
quat = tf.transformations.quaternion_from_euler(math.radians(0.0), math.radians(0.0), math.radians(0.0))
pose.pose.orientation.x = quat[0]
pose.pose.orientation.y = quat[1]
pose.pose.orientation.z = quat[2]
pose.pose.orientation.w = quat[3]
G.pose_to_pub = pose

G.pose_publish_ok = True

G.tick = 0

while G.tick < 50:
	print "Rotate"
	# G.att_pub.publish(G.target_att_to_pub)
	G.position_pub.publish(G.pose_to_pub)
	G.rate.sleep()
	G.tick += 1

# target = mavros_msgs.msg.PositionTarget()
# target.position.x = 0.0
# target.position.y = 10.0
# target.position.z = 10.0
# target.coordinate_frame = target.FRAME_LOCAL_OFFSET_NED
# target.type_mask = target.IGNORE_VX | target.IGNORE_VY | target.IGNORE_VZ | target.IGNORE_AFX | target.IGNORE_AFY | target.IGNORE_AFZ | target.FORCE | target.IGNORE_YAW | target.IGNORE_YAW_RATE
# G.target_to_pub = target

G.tick = 0

pose = geometry_msgs.msg.PoseStamped()
pose.pose.position.x = 10.0
pose.pose.position.y = 0.0
pose.pose.position.z = 10.0
quat = tf.transformations.quaternion_from_euler(math.radians(0.0), math.radians(0.0), math.radians(0.0))
pose.pose.orientation.x = quat[0]
pose.pose.orientation.y = quat[1]
pose.pose.orientation.z = quat[2]
pose.pose.orientation.w = quat[3]
G.pose_to_pub = pose


while G.tick < 100:
	print "Move forward 10m"
	G.position_pub.publish(G.pose_to_pub)
	# G.position_pub.publish(G.target_to_pub)
	G.rate.sleep()
	G.tick += 1


# G.set_mode(0, "AUTO.LAND")
while not G.curr_state.mode == "AUTO.LAND":
	print "Trying to land!"
	G.set_mode(0, "AUTO.LAND")
	# G.position_pub.publish(G.pose_to_pub)
	G.rate.sleep()

print "Landing"

# print "Flying"

rospy.spin()
